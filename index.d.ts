export {};
declare global{
    namespace NodeJS{
        interface Global{
            appConfig:{
                envName:string
                baseURL:string
                swaggerURL:string
                users:string
                maxResponseTimeMs:number
            }
        }
    }
}