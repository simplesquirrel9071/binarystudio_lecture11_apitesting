import { ApiRequest } from "../request";
import { ArticleComment } from "tests/api/specs/data/models/Comment";

export class ArticleCommentController {
    baseURL:string = global.appConfig.baseURL
    
    createArticleComment = async (bearerToken:string, comment:ArticleComment) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("POST")
                                        .url("article_comment").body(comment)
                                        .bearerToken(bearerToken).send()

    getCommentsByArticleId = async (bearerToken:string, articleId:string) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("GET")
                                        .url(`article_comment/of/${articleId}`)
                                        .searchParams({
                                            size:200
                                        }).bearerToken(bearerToken).send()
}
