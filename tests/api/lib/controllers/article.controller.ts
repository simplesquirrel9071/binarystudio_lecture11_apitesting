import { ApiRequest } from "../request";
import { Article } from "../../specs/data/models/Article"

export class ArticleController{
    baseURL:string = global.appConfig.baseURL
    
    createArticle = async (bearerToken:string, article:Article) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("POST")
                                        .url("article").body(article)
                                        .bearerToken(bearerToken).send()

    getAuthorArticles = async (bearerToken:string) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("GET")
                                        .url("article/author")
                                        .bearerToken(bearerToken).send()

    getArticleById = async (bearerToken:string, articleId:string) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("GET")
                                        .url(`article/${articleId}`)
                                        .bearerToken(bearerToken).send()
}