import { Author } from "tests/api/specs/data/models/Author";
import { ApiRequest } from "../request";

export class AuthorController {
    baseURL:string = global.appConfig.baseURL
    
    setupAuthor = async (bearerToken:string, author:Author) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("POST")
                                        .url("author").body(author)
                                        .bearerToken(bearerToken).send()

    getAuthorSettings = async (bearerToken:string) => await new ApiRequest()
                                        .prefixUrl(this.baseURL)
                                        .method("GET").url("author")
                                        .bearerToken(bearerToken).send()

    getPublicAuthor = async (bearerToken:string, authorId:string) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("GET")
                                        .url(`author/overview/${authorId}`)
                                        .bearerToken(bearerToken).send()
}