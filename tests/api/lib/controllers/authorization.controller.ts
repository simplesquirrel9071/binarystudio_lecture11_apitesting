import { ApiRequest } from "../request";

export class AuthorizationController{
    baseURL:string = global.appConfig.baseURL
    
    login = async (email, password) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("POST")
                                        .url("auth/login").body({
                                            "email":email,
                                            "password":password
                                        }).send()
    
}