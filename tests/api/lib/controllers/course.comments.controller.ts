import { ApiRequest } from "../request";
import { CourseComment } from "tests/api/specs/data/models/Comment";

export class CourseCommentController{
    baseURL:string = global.appConfig.baseURL
    
    createCourseComment = async (bearerToken:string, comment:CourseComment) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("POST")
                                        .url("course_comment").body(comment)
                                        .bearerToken(bearerToken).send()

    getCourseCommentsById = async (bearerToken:string, courseId:string) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("GET")
                                        .url(`course_comment/of/${courseId}`)
                                        .searchParams({
                                            size:200
                                        }).bearerToken(bearerToken).send()

}