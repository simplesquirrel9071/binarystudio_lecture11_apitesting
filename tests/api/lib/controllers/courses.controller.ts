import { ApiRequest } from "../request";

export class CoursesController {
    baseURL:string = global.appConfig.baseURL

    async getAllCourses() {
        const response = await new ApiRequest()
            .prefixUrl(this.baseURL)
            .method("GET")
            .url(`course/all`) 
            .send();
        return response;
    }

    async getPopularCourses() {
        const response = await new ApiRequest()
            .prefixUrl(this.baseURL)
            .method("GET")
            .url(`course/popular`)
            .send();
        return response;
    }

    async getAllCourseInfoById(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(this.baseURL)
            .method("GET")
            .url(`course/${id}/info`) 
            .send();
        return response;
    }
}
