import { Student } from "tests/api/specs/data/models/Student";
import { ApiRequest } from "../request";

export class StudentController {
    baseURL:string = global.appConfig.baseURL

    setupStudent = async (bearerToken:string, student:Student) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("POST")
                                        .url(`student`).body(student)
                                        .bearerToken(bearerToken).send()
    
    getStudentSettings = async (bearerToken:string) => await new ApiRequest()
                                        .prefixUrl(this.baseURL)
                                        .method("GET").url(`student`)
                                        .bearerToken(bearerToken).send()
}