import { ApiRequest } from "../request";

export class UserController{
    baseURL:string = global.appConfig.baseURL
    
    getCurrentUser = async (bearerToken:string) => await new ApiRequest()
                                        .prefixUrl(this.baseURL).method("GET")
                                        .url("user/me")
                                        .bearerToken(bearerToken).send()
    
}