import { expect } from "chai"
import { checkBodyAttributeToBeEqual, checkBodyMessage, checkBodyStatus, checkBodyStatusCode, checkInvalidLoginResponse, checkResponseJSONSchema, checkResponseTime, checkStatusCode } from "../../helpers/responseChecker"
import { ArticleCommentController } from "../lib/controllers/article.comment.controller"
import { ArticleController } from "../lib/controllers/article.controller"
import { AuthorController } from "../lib/controllers/author.controller"
import { AuthorizationController } from "../lib/controllers/authorization.controller"
import { UserController } from "../lib/controllers/user.controller"
import { Article } from "./data/models/Article"
import { Author } from "./data/models/Author"
import { ArticleComment } from "./data/models/Comment"
import { invalidLoginData } from "./data/testData/loginTestData"

var chai = require('chai')
chai.use(require('chai-json-schema'));

const auth = new AuthorizationController()
const author = new AuthorController()
const user = new UserController()
const article = new ArticleController()
const articleComment = new ArticleCommentController()

const loginScheme = require("./data/jsonSchemas/loginScheme.json")
const currentUserScheme = require("./data/jsonSchemas/currentUserScheme.json")
const allArticleCommentsScheme = require("./data/jsonSchemas/allArticleCommentsScheme.json")
const allArticlesScheme = require("./data/jsonSchemas/allArticlesScheme.json")
const articleCommentScheme = require("./data/jsonSchemas/articleCommentScheme.json")
const authorOverviewScheme = require("./data/jsonSchemas/authorOverviewScheme.json")
const createdArticleScheme = require("./data/jsonSchemas/createdArticleScheme.json")
const detailArticleScheme = require("./data/jsonSchemas/detailArticleScheme.json")

const EMAIL = global.appConfig.users.author.email
const PASSWORD = global.appConfig.users.author.password

describe("Author end-to-end", () => {
    let accessToken: string
    let authorData: Author = {
        "avatar": "https://i.pinimg.com/736x/67/f8/53/67f8532cdd1e3a69c5efa4f1aebf9336.jpg",
        "biography": "biographybiographybiographybiographybiographybiographybiographybiographybiographybiography",
        "company": "Meme enterprise",
        "firstName": "VolanGdeRot",
        "job": "Co-founder",
        "lastName": "OnTut",
        "location": "Zambia",
        "twitter": "",
        "website": ""
    }
    let authorId:string
    let articleData:Article
    let articleId:string
    let articleCommentData:ArticleComment
    let articleCommentId:string

    before("getAccessToken", async () =>{
        let response = await auth.login(EMAIL,PASSWORD)
        accessToken = response.body.accessToken

        checkStatusCode(response, 200)
        checkResponseTime(response)
        expect(response.body.accessToken.length, `Access token length should be 215 symbols`).to.be.equal(215)
        expect(response.body.refreshToken.length, `Refresh token length should be 215 symbols`).to.be.equal(215)      
        checkResponseJSONSchema(response, loginScheme)
    })

    it(`setupAuthor`, async function () {
        if(await (await author.getAuthorSettings(accessToken)).body.avatar === authorData.avatar)
            this.skip();

        let response = await author.setupAuthor(accessToken, authorData)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkBodyStatus(response, "Success. Your profile has been updated.")
    })

    invalidLoginData.forEach(el => {
        it(`invalidLogin('${el.email}' | '${el.password}' )`, async () => {
            let response = await auth.login(el.email,el.password)
            checkInvalidLoginResponse(response)
        })
    });

    it(`getCurrentUser`, async () => {
        let response = await user.getCurrentUser(accessToken)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkBodyAttributeToBeEqual(response, "email", EMAIL)
        checkBodyAttributeToBeEqual(response, "avatar", authorData.avatar)
        checkBodyAttributeToBeEqual(response, "emailVerified", true)
        expect(response.body.id, `Author id should not be null`).not.to.be.equal(null)
        expect(response.body.role.name, `Author role should be AUTHOR`).to.be.equal("AUTHOR")
        checkResponseJSONSchema(response, currentUserScheme)
    })

    it(`getAuthorOverview`, async () => {
        authorId = await (await author.getAuthorSettings(accessToken)).body.id
        let response = await author.getPublicAuthor(accessToken, authorId)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        expect(response.body.userId, `Author userId should not be null`).not.to.be.equal(null)
        expect(response.body.id, `Author id should not be null`).not.to.be.equal(null)
        checkBodyAttributeToBeEqual(response, "firstName", authorData.firstName)
        checkBodyAttributeToBeEqual(response, "lastName", authorData.lastName)
        checkBodyAttributeToBeEqual(response, "avatar", authorData.avatar)
        checkBodyAttributeToBeEqual(response, "biography", authorData.biography)
        checkResponseJSONSchema(response, authorOverviewScheme)
    })

    it(`createArticle`, async () => {
        articleData = {
            "authorId":authorId,
            "authorName":`${authorData.firstName} ${authorData.lastName}`,
            "image":"https://st2.depositphotos.com/1350793/8441/i/600/depositphotos_84416316-stock-photo-hand-pointing-to-online-course.jpg",
            "name":"From trainee to lead in 1 cup of tea",
            "text":"It's impossible :("
        }

        let response = await article.createArticle(accessToken, articleData)
        articleId = response.body.id

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkBodyAttributeToBeEqual(response, "name", articleData.name)
        checkBodyAttributeToBeEqual(response, "text", articleData.text)
        checkBodyAttributeToBeEqual(response, "image", articleData.image)
        checkBodyAttributeToBeEqual(response, "authorId", articleData.authorId)
        checkBodyAttributeToBeEqual(response, "authorName", `${authorData.firstName} ${authorData.lastName}`)
        checkResponseJSONSchema(response, createdArticleScheme)

    })

    it(`getAllAuthorArticles`, async () => {
        let response = await article.getAuthorArticles(accessToken)
        let createdArticle = response.body.find(el => el.id === articleId)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        expect(createdArticle.id, `Course comment id should be ${articleId}`).to.be.equal(articleId)
        expect(response.body.id, `Article id should not be null`).not.to.be.equal(null)
        for(const [key,value] of Object.entries(createdArticle)){
            if(key!=="id")
                expect(createdArticle[key], `Course comment ${key} should be ${value}`).to.be.equal(value)
        }
        checkResponseJSONSchema(response, allArticlesScheme)
    })

    it(`getArticleById`, async () => {
        let response = await article.getArticleById(accessToken, articleId)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkBodyAttributeToBeEqual(response, "name", articleData.name)
        checkBodyAttributeToBeEqual(response, "text", articleData.text)
        checkBodyAttributeToBeEqual(response, "image", articleData.image)
        checkBodyAttributeToBeEqual(response, "favourite", false)

        expect(response.body.author.id,`Article author id should be ${authorId}`).to.be.equal(authorId)
        expect(response.body.author.name,`Article author name should be ${authorData.firstName} ${authorData.lastName}`).to.be.equal(`${authorData.firstName} ${authorData.lastName}`)
        expect(response.body.author.biography,`Article biography should be ${authorData.biography}`).to.be.equal(authorData.biography)
        expect(response.body.author.avatar,`Article avatar should be ${authorData.avatar}`).to.be.equal(authorData.avatar)
        expect(response.body.author.articles.length,`Article author artciles amount should be more than 0`).to.be.above(0)
        checkResponseJSONSchema(response, detailArticleScheme)
    })

    it(`createCommentToArticle`, async () => {
        articleCommentData = {
            "articleId":articleId,
            "text":"Very informativniy information v article napisan by avtor"
        }

        let response = await articleComment.createArticleComment(accessToken, articleCommentData)
        articleCommentId = response.body.id

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkBodyAttributeToBeEqual(response, "text", articleCommentData.text)
        checkBodyAttributeToBeEqual(response, "articleId", articleCommentData.articleId)
        expect(response.body.id, `Article comment id should not be null`).not.to.be.equal(null)
        checkResponseJSONSchema(response, articleCommentScheme)
    })

    it(`getCommentsByArticleId`, async () => {
        let response = await articleComment.getCommentsByArticleId(accessToken, articleId)
        let createdComment = response.body.find(el=>el.id === articleCommentId)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        expect(response.body.length, `Article comment amount should not be more than 0`).to.be.above(0)
        for(const [key,value] of Object.entries(createdComment)){
            if(key!=="id")
                expect(createdComment[key], `Course comment ${key} should be ${value}`).to.be.equal(value)
        }
        expect(createdComment.id, `Course comment id should be ${articleCommentId}`).to.be.equal(articleCommentId)
        checkResponseJSONSchema(response, allArticleCommentsScheme)
    })
})