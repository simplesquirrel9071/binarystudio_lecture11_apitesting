export interface Article {
    authorId: string
    authorName: string
    image: string
    name: string
    text: string
  }