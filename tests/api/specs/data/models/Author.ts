export interface Author {
    avatar:string
    biography: string
    company: string
    firstName: string
    job: string
    lastName: string
    location: string
    twitter: string
    website: string
}