export interface ArticleComment {
    articleId:string,
    text:string
}

export interface CourseComment {
    courseId:string,
    text:string
}