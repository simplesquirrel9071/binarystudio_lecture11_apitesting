export interface Student {
    avatar:string,
    biography: string,
    company: string,
    direction: "Developer" | "IT Professional" | "Creative" | "Other",
    education: "Middle School" | "High School" | "Bachelor's degree" | "Master's degree",
    employment: "Employee" | "Self-employed",
    experience: number,
    firstName: string,
    industry: "Consulting" | "Web Services" | "Software Products" | "Telecommunications" | "Government" | "Education",
    job: string,
    lastName: string,
    level: "Beginner" | "Intermediate" | "Advenced",
    location: string,
    role: "DevOps" | "IT Support" | "Full-stack Game Dev" | "Mobile Dev" | "Manager" | "QA" | 
        "Backend Dev" | "Frontend Dev" | "Full-stack Dev" | "Graphic Designer" | "UI/UX Designer",
    tags: Array<any>,
    website: string,
    year: number
}

