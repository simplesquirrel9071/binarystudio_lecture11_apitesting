import { expect } from "chai"
import { AuthorizationController } from "../lib/controllers/authorization.controller"
import { UserController } from "../lib/controllers/user.controller"
import { CoursesController } from "../lib/controllers/courses.controller"

import { Student } from "./data/models/Student"
import { CourseComment } from "./data/models/Comment"
import { StudentController } from "../lib/controllers/student.controller"
import { CourseCommentController } from "../lib/controllers/course.comments.controller"
import { checkBodyAttributeToBeEqual, checkBodyMessage, checkInvalidLoginResponse, checkResponseJSONSchema, checkResponseTime, checkStatusCode } from "../../helpers/responseChecker"

import { invalidLoginData } from "./data/testData/loginTestData"

var chai = require('chai')
chai.use(require('chai-json-schema'));

const student = new StudentController()
const user = new UserController()
const course = new CoursesController()
const courseComment = new CourseCommentController()
const auth = new AuthorizationController()

const loginScheme = require("./data/jsonSchemas/loginScheme.json")
const currentUserScheme = require("./data/jsonSchemas/currentUserScheme.json")
const allCourseCommentsScheme = require("./data/jsonSchemas/allCourseCommentsScheme.json")
const courseCommentScheme = require("./data/jsonSchemas/courseCommentScheme.json")
const studentSettingsScheme = require("./data/jsonSchemas/studentSettingScheme.json")

const EMAIL = global.appConfig.users.student.email
const PASSWORD = global.appConfig.users.student.password


describe("Student end-to-end", async () => {
    var accessToken: string = ""
    let studentData:Student = {
        "avatar": "https://i.pinimg.com/736x/67/f8/53/67f8532cdd1e3a69c5efa4f1aebf9336.jpg",
        "biography": "biographybiographybiographybiographybiographybiographybiographybiographybiographybiography",
        "company": "Meme enterprise",
        "direction": "IT Professional",
        "level": "Advenced",
        "education":"Master's degree",
        "employment":"Employee",
        "experience":5,
        "firstName":"SupermEn",
        "industry":"Software Products",
        "job":"Human",
        "lastName":"Pelmen",
        "location":"Ukraine",
        "role":"QA",
        "website":"",
        "year":1999,
        "tags":[],
    }
    let courseId:string
    let courseCommentData:CourseComment
    let courseCommentId:string
    

    before("getAccessToken", async () =>{
        let response = await auth.login(EMAIL,PASSWORD)
        accessToken = response.body.accessToken

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkResponseJSONSchema(response, loginScheme)
        expect(response.body.accessToken.length, `Access token length should be 215 symbols`).to.be.equal(215)
        expect(response.body.refreshToken.length, `Refresh token length should be 215 symbols`).to.be.equal(215)
    })

    invalidLoginData.forEach(el => {
        it(`invalidLogin('${el.email}' | '${el.password}' )`, async () => {
            let response = await auth.login(el.email,el.password)
            checkInvalidLoginResponse(response)
        })
    });
    

    it(`setupStudent`, async function() {
        if(await (await student.getStudentSettings(accessToken)).body.avatar === studentData.avatar)
            this.skip();
            
        let response = await student.setupStudent(accessToken, studentData)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkBodyMessage(response, "Success. Your profile has been updated.")
    })

    it(`getStudentSettings`, async () => {
        let response = await student.getStudentSettings(accessToken)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        for(const [key,value] of Object.entries(studentData)){
            if(key!=="tags")
                expect(response.body[key], `Course comment ${key} should be ${value}`).to.be.equal(value)
        }
        checkResponseJSONSchema(response, studentSettingsScheme)
    })

    it(`getCurrentUser`, async () => {
        let response = await user.getCurrentUser(accessToken)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkBodyAttributeToBeEqual(response, "email", EMAIL)
        checkBodyAttributeToBeEqual(response, "avatar", studentData.avatar)
        checkBodyAttributeToBeEqual(response, "emailVerified", true)
        expect(response.body.id, `Student id should not be null`).not.to.be.equal(null)
        expect(response.body.role.name, `Student role should be USER`).to.be.equal("USER")
        checkResponseJSONSchema(response, currentUserScheme)
    })

    it(`createCourseComment`, async () => {
        courseId = await (await course.getAllCourses()).body[0].id
        courseCommentData = {
            courseId:courseId,
            text:"Super comment"
        }

        let response = await courseComment.createCourseComment(accessToken, courseCommentData)
        courseCommentId = response.body.id

        checkStatusCode(response, 200)
        checkResponseTime(response)
        checkBodyAttributeToBeEqual(response, "text", courseCommentData.text)
        checkBodyAttributeToBeEqual(response, "courseId", courseCommentData.courseId)
        expect(response.body.id, `Course comment id should not be null`).not.to.be.equal(null)
        checkResponseJSONSchema(response, courseCommentScheme)

    })

    it(`getCommentsByCourseId`, async () => {
        let response = await courseComment.getCourseCommentsById(accessToken, courseId)
        let createdComment = response.body.find(el=>el.id === courseCommentId)

        checkStatusCode(response, 200)
        checkResponseTime(response)
        expect(response.body.length, `Course comment amount should not be more than 0`).to.be.above(0)
        for(const [key,value] of Object.entries(createdComment)){
            if(key!=="id")
                expect(createdComment[key], `Course comment ${key} should be ${value}`).to.be.equal(value)
        }
        expect(createdComment.id, `Course comment id should be ${courseCommentId}`).to.be.equal(courseCommentId)
        checkResponseJSONSchema(response, allCourseCommentsScheme)
    })
})