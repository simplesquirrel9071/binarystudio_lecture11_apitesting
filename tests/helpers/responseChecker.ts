import {expect} from "chai"
import { allowedStatusCodes } from "tests/api/specs/data/models/StatusCode"

export const checkStatusCode = (response, statusCode: allowedStatusCodes) => {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.be.equal(statusCode)
}

export const checkResponseTime = (response, maxResponseTimeMs: number = global.appConfig.maxResponseTimeMs) => {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTimeMs}ms`).to.be.lessThan(maxResponseTimeMs)
}

export const checkResponseJSONSchema = (response, jsonSchema) => {
    expect(response.body).to.be.jsonSchema(jsonSchema); 
}

export const checkBodyStatusCode = (response, statusCode: allowedStatusCodes) => {
    expect(response.body.statusCode, `Body Status Code should be '${statusCode}'`).to.be.equal(statusCode)
}

export const checkBodyStatus = (response, status:string) => {
    expect(response.body.status, `Body Status should be '${status}'`).to.be.equal(status)
}

export const checkBodyMessage = (response, message:string) => {
    expect(response.body.message, `Body Message should be '${message}'`).to.be.equal(message)
}

export const checkBodyAttributeToBeEqual = (response, attribute, expectedResult) => {
    expect(response.body[attribute], `Body ${attribute} should be '${expectedResult}'`).to.be.equal(expectedResult)
}

export const checkInvalidLoginResponse = (response) => {
    checkStatusCode(response, 401)
    checkResponseTime(response)
    checkBodyStatusCode(response, 401)
    checkBodyStatus(response,"UNAUTHORIZED")
    checkBodyMessage(response, "Bad credentials")
}